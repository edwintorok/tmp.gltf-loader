#!/bin/sh
set -e
ocamlbuild -use-ocamlfind gen/jsongen.native -tag debug
(cd src && ../jsongen.native ../../glTF/specification/glTF.schema.json)
ocamlbuild -use-ocamlfind src/glTF.cmx
ocaml setup.ml -doc -use-ocamlfind
make
