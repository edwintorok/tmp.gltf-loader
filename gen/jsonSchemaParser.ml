open Ezjsonm
open Jsonparsercommon

let uri_of_json json = Uri.of_string (get_string json)
type t = {
  type_: type_;
  properties: (string * t) list;
  pattern_properties: (string * t) list;
  additional_properties: additional_properties;
  items: items;
  additional_items: additional_items;
  required: bool;
  dependencies: dependencies;
  minimum: float option;
  exclusive_minimum: bool;
  maximum: float option;
  exclusive_maximum: bool;
  min_items: int;
  max_items: int option;
  unique_items: bool;
  pattern: string option;
  min_length: int;
  max_length: int option;
  enum: string list option;
  default: json option;
  title: string option;
  description: string option;
  format_: string option;
  divisible_by: float;
  disallow: disallow option;
  extends: extends;
  id: Uri.t option;
  ref: Uri.t option;
  schema: Uri.t option;
}

and type__items =
  [ `String of string
  | `Schema of t
  ]

and type_ =
  [ `String of string
  | `A of type__items list
  ]

and additional_properties =
  [ `Bool of bool
  | `Schema of t
  ]

and items =
  [ `A of t list
  | `Schema of t
  ]

and additional_items =
  [ `Bool of bool
  | `Schema of t
  ]

and dependencies = {
  type_: dependencies_type option;
  items: string option
}

and dependencies_type =
  [ `String of string
  | `A of string list
  | `Schema of t
  ]

and disallow =
  [ `String of string
    | `A of disallow_items list]

and disallow_items =
  [ `String of string
    | `Schema of t ]

and extends =
  [ `A of t list
    | `Schema of t]

let rec type__items_of_json = function
  | `String _ as v -> v
  | json ->
      `Schema (schema_of_json json)

and type__of_json = function
  | `String _ as v -> v
  | `A a ->
      `A (List.rev (List.rev_map type__items_of_json a))
  | json ->
      raise (Parse_error(json, "string or array expected"))

and additional_properties_of_json = function
  | `Bool _ as v -> v
  | json ->
      `Schema (schema_of_json json)

and items_of_json = function
  | `A a ->
      `A (List.rev (List.rev_map schema_of_json a))
  | json ->
      `Schema (schema_of_json json)

and additional_items_of_json = function
  | `Bool _ as v -> v
  | json ->
      `Schema (schema_of_json json)

and dependencies_of_json json =
  let obj = get_dict json in
  {
    type_ = get_field_opt dependencies_type__of_json obj "type";
    items = get_field_opt get_string obj "items"
  }

and dependencies_type__of_json = function
  | `String _ as v -> v
  | `A a ->
      `A (List.rev (List.rev_map get_string a))
  | json ->
      `Schema (schema_of_json json)

and disallow_of_json = function
  | `String _ as v -> v
  | `A a ->
      `A (List.rev (List.rev_map disallow_items_of_json a))
  | json ->
      raise (Parse_error (json, "string or array expected"))

and disallow_items_of_json = function
  | `String _ as v -> v
  | json ->
      `Schema (schema_of_json json)

and extends_of_json = function
  | `A a ->
      `A (List.rev (List.rev_map schema_of_json a))
  | json ->
      `Schema (schema_of_json json)

and empty_dependencies = {
  type_ = None;
  items = None
}

and empty_schema = {
  type_ = `String "any";
  properties = [];
  pattern_properties = [];
  additional_properties = `Bool false;
  items = `Schema empty_schema;
  additional_items = `Schema empty_schema;
  required = false;
  dependencies = empty_dependencies;
  minimum = None;
  exclusive_minimum = false;
  maximum = None;
  exclusive_maximum = false;
  min_items = 0;
  max_items = None;
  unique_items = false;
  pattern = None;
  min_length = 0;
  max_length = None;
  enum = None;
  default = None;
  title = None;
  description = None;
  format_ = None;
  divisible_by = 1.0;
  disallow = None;
  extends = `Schema empty_schema;
  id = None;
  ref = None;
  schema = None;
}

and schema_of_json json =
  let obj = get_dict json in
  {
    (* TODO: $ref handling *)
    type_ = get_field ~default:(`String "any") type__of_json obj "type";
    properties =
      dict_map schema_of_json (get_field ~default:[] get_dict obj "properties");
    pattern_properties =
      dict_map schema_of_json (get_field ~default:[] get_dict obj "patternProperties");
    additional_properties = get_field ~default:(`Bool false)
      additional_properties_of_json obj "additionalProperties";
    items =
      get_field ~default:(`Schema empty_schema) items_of_json obj "items";
    additional_items =
      get_field ~default:(`Schema empty_schema) additional_items_of_json obj "items";
    required =
      get_field ~default:false get_bool obj "required";
    dependencies =
      get_field ~default:empty_dependencies dependencies_of_json obj
        "dependencies";
    minimum = get_field_opt get_float obj "minimum";
    maximum = get_field_opt get_float obj "maximum";
    exclusive_minimum = get_field ~default:false get_bool obj "exclusiveMinimum";
    exclusive_maximum = get_field ~default:false get_bool obj "exclusiveMaximum";
    min_items = get_field ~default:0 get_int obj "minItems";
    max_items = get_field_opt get_int obj "maxItems";
    unique_items = get_field ~default:false get_bool obj "uniqueItems";
    pattern = get_field_opt get_string obj "pattern";
    min_length = get_field ~default:0 get_int obj "minLength";
    max_length = get_field_opt get_int obj "maxLength";
    enum = get_field_opt get_strings obj "enum"; (* TODO: validate uniqueItems
  and minitems *)
    default = get_field_opt json_of_json obj "default";
    title = get_field_opt get_string obj "title";
    description = get_field_opt get_string obj "description";
    format_ = get_field_opt get_string obj "format";
    divisible_by = get_field ~default:1.0 get_float obj "divisibleBy"; (* TODO:
      validate*)
    disallow = get_field_opt disallow_of_json obj "disallow";
    extends = get_field ~default:(`Schema empty_schema) extends_of_json obj "extends";
    id = get_field_opt uri_of_json obj "id";
    ref = get_field_opt uri_of_json obj "$ref";
    schema = get_field_opt uri_of_json obj "$schema"
  }
