open JsonSchemaParser
open Ocamlgen
open Format

(*
let warn =
  Printf.ksprintf (fun s ->
    Printf.eprintf "Warning: %s\n%!" s
  )
  *)

module StringSet = Set.Make(Identifier.Capitalized)
let modules_once = ref StringSet.empty

type dest = {
  ml_ch: out_channel;
  ml: formatter;
  mli: formatter;
  mli_ch: out_channel;
}

let output_parser dest t doc =
  fprintf dest.ml "@,";
  pp_type_decls dest.ml [t];
  pp_parser dest.ml t;
  fprintf dest.mli "@,";
  pp_type_sig dest.mli t doc;
  pp_parser_sig dest.mli t

let typedef typename id =
  Expr (Tname (Identifier.v id))

let typedef_expr typename e =
  Expr e

let is_empty schema =
  schema.ref = None && schema.type_ = `String "any" &&
  schema.properties = []

let rec generate_schema dest context typename (schema:t) =
  match schema.ref, schema.additional_properties, schema.type_ with
  | Some ref, _, _ ->
      ref_schema dest context typename ref
  | None, `Bool true, _ ->
      failwith "Invalid additionalProperties: true"
  | None, `Schema schema, _ ->
      begin match generate_schema dest context typename schema with
      | Record _ as typedecl ->
          let id = Identifier.v typename in
          output_parser dest (id, typedecl) schema.description;
          fprintf dest.ml "@,";
          fprintf dest.mli "@,";
          Expr (TMap (Tname id))
      | Expr e ->
          Expr (TMap e)
      end
  | None, _, `String "object" ->
      object_schema dest context typename schema
  | None, _, `String "any" -> typedef typename "json"
  | None, _, `String "string" -> typedef_expr typename (TString None)
  | None, _, `String "number" -> typedef_expr typename (TFloat None)
  | None, _, `String "integer" -> typedef_expr typename (TInt64 None)
  | None, _, `String "boolean" -> typedef_expr typename (TBool None)
  | None, _, `String "array" ->
      begin match schema.items with
      | `A _ -> failwith "TODO: array items"
      | `Schema schema ->
          match generate_schema dest context typename schema with
          | Record _ as typedecl ->
            let id = Identifier.v typename in
            output_parser dest (id, typedecl) schema.description;
            fprintf dest.ml "@,";
            fprintf dest.mli "@,";
            Expr (TArray (Tname id))
          | Expr (TOption e) ->
            Expr (TArray e)
          | Expr e ->
            Expr (TArray e)
      end
  | None, _, `String t -> failwith ("TODO: " ^ t)
  | None, _, `A types ->
      List.iter (function
        | `String _ -> ()
        | `Schema schema ->
          match generate_schema dest context typename schema with
          | Record _ as typedecl ->
            let id = Identifier.v typename in
            output_parser dest (id, typedecl) schema.description;
            fprintf dest.ml "@,";
            fprintf dest.mli "@,"
          | Expr _ -> ()
      ) types;
      (* TODO: proper union *)
      typedef typename "json"

and object_schema dest context typename schema =
  let fields = List.rev (List.rev_map (fun (name, schema) ->
    let typ = match generate_schema dest context name schema with
      | Record _ as typedecl ->
          let id = Identifier.v name in
          output_parser dest (id, typedecl) schema.description;
          fprintf dest.ml "@,";
          fprintf dest.mli "@,";
          Tname id
      | Expr e ->
        e in
    let open Ezjsonm in
    let expr = match schema.required, schema.default, typ with
    | true, _, _ | _, _, TMap _ -> typ
    | _, _, Tname t when t = Identifier.v "json" -> typ
    | false, None, _ ->  TOption typ
    | false, Some default, TString _ -> TString (Some (get_string default))
    | false, Some default, TFloat _ -> TFloat (Some (get_float default))
    | false, Some default, TInt64 _ ->
        TInt64 (Some (Int64.of_int (get_int default)))
    | false, Some default, TBool _ -> TBool (Some (get_bool default))
    | false, Some default, _ ->
        failwith "this default value not supported"
    in
    name, expr, schema.description
    ) schema.properties) in
  Record fields

and ref_schema dest context typename ref =
  let path = Uri.path (Uri.resolve "" context ref) in
  let base = Filename.chop_suffix (Filename.basename path) ".schema.json" in
  let modulename = Identifier.Capitalized.v base in
  if not (StringSet.mem modulename !modules_once) then begin
    modules_once := StringSet.add modulename !modules_once;
    generate dest ~modulename:(Some modulename) path;
  end;
  let id = Identifier.prefix modulename (Identifier.v "t") in
  Expr (Tname id)

and generate dest ?(modulename = None) schemafile =
  let ch = open_in schemafile in
  let context = Uri.of_string (Filename.dirname schemafile ^ "/") in
  begin try
    Printf.eprintf "Parsing schema %s ..%!" schemafile;
    let json = Ezjsonm.from_channel ch in
    Printf.eprintf ".%!";
    let schema = schema_of_json json in
    Printf.eprintf " OK\n%!";
    Printf.eprintf "Generating JSON parser ...%!";
    let decls = generate_schema dest context "t" schema in
    begin match modulename with
    | None -> ()
    | Some modulename ->
      fprintf dest.ml "@,@[<v 2>module %a = struct" Identifier.Capitalized.pp modulename;
      fprintf dest.mli "@,@[<v 2>module %a : sig" Identifier.Capitalized.pp modulename
    end;
    output_parser dest (Identifier.v "t", decls) schema.description;
    begin match modulename with
    | None -> ()
    | Some _ ->
        fprintf dest.ml "@]@,end@,";
        fprintf dest.mli "@]@,end@,"
    end;
    Printf.eprintf " OK\n%!"
  with Ezjsonm.Parse_error (json, msg) as e ->
    if Printexc.backtrace_status () then
      Printexc.print_backtrace stderr;
    Printf.eprintf "%s in '%s'\n%!" msg
      (Ezjsonm.to_string (`A [json]));
    raise Exit
  end;
  close_in ch

let generate_top schemafile =
  let base = Filename.chop_suffix (Filename.basename schemafile) ".schema.json" in
  let ml_ch = open_out (base ^ ".ml") in
  let mli_ch = open_out (base ^ ".mli") in
  let dest = {
    ml_ch; mli_ch;
    ml = formatter_of_out_channel ml_ch;
    mli = formatter_of_out_channel mli_ch
  } in
  fprintf dest.ml "@[<v>(* Generated by json-schema. DO NOT EDIT. *)@,";
  fprintf dest.ml "open Jsonparsercommon@,";
  fprintf dest.ml "open Ezjsonm@,";
  fprintf dest.ml "(** *)";

  fprintf dest.mli "@[<v>(* Generated by json-schema. DO NOT EDIT. *)@,";
  fprintf dest.mli "@[open Jsonparsercommon@]@,";
  fprintf dest.mli "(** *)";

  generate dest schemafile;
  fprintf dest.ml "@]@.";
  fprintf dest.mli "@]@.";
  close_out dest.ml_ch;
  close_out dest.mli_ch

let () =
  let usage_msg = Printf.sprintf "%s [file...]" Sys.argv.(0) in
  Arg.(parse (align []) generate_top usage_msg)

(* TODO:
  * better error messages (show full json reference up to invalid field)
  * permissive parsing mode (missing values get substituted defaults, etc.)
  * validate additional fields
  * only accept uncapitalized (non-prefixed) identifiers for record fields
  * *)
