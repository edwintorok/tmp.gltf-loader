open Ezjsonm
type json = Ezjsonm.t
type obj = (string * json) list
module StringMap = Map.Make(String)

let get_boolean = function
  | `Bool b -> b
  (* be permissive, accept 0|1 as boolean too *)
  | `Float 1.0 -> true
  | `Float 0.0 -> false
  | json ->
      raise (Parse_error (json, "boolean required"))

let get_field_opt f obj field =
  try Some (f (List.assoc field obj))
  with
  | Not_found -> None
  | Parse_error _ as e ->
    if Printexc.backtrace_status () then
      Printf.eprintf "field '%s' in \n%!" field;
  raise e

let get_field ~default f obj field =
  match get_field_opt f obj field with
  | Some v -> v
  | None -> default

let get_field_required f obj field =
  match get_field_opt f obj field with
  | Some v -> v
  | None ->
      raise (Parse_error (`O obj, Printf.sprintf "field '%s' is required" field))

let get_int64 json =
  (* TODO: no fractional part *)
  Int64.of_float (get_float json)

let json_of_json x = x

let dict_map f dict =
  List.rev (List.rev_map (fun (k, v) ->
    k, f v
  ) dict)

let get_strings_array json = Array.of_list (get_strings json)
let get_array f = function
  | `A a ->
      Array.of_list (List.rev (List.rev_map f a))
  | json ->
      raise (Parse_error (json, "array expected"))

let get_field_dict f obj field =
  let dict = get_field ~default:[] get_dict obj field in
  List.fold_left (fun map (k, v) ->
    StringMap.add k (f v) map
  ) StringMap.empty dict

let get_dict_map f obj =
  let dict = get_dict obj in
  List.fold_left (fun map (k, v) ->
    StringMap.add k (f v) map
  ) StringMap.empty dict

let check_unique_items json l =
  let rec check_duplicates = function
    | a :: b :: _ when a = b ->
        raise (Parse_error (json, "Items must be unique"))
    | _ :: tl -> check_duplicates tl
    | [] -> ()
  in
  check_duplicates (List.fast_sort Pervasives.compare l)

