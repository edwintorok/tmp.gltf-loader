type json = Ezjsonm.t
type obj = (string * json) list
module StringMap: module type of Map.Make(String)
val get_field_opt : (json -> 'a) -> obj -> string -> 'a option
val get_field : default:'a -> (json -> 'a) -> obj -> string -> 'a
val get_field_required : (json -> 'a) -> obj -> string -> 'a
val get_field_dict: (json -> 'a) -> obj -> string -> 'a StringMap.t
val get_array: (json -> 'a) -> json -> 'a array
val get_int64 : json -> int64
val json_of_json : json -> json
val dict_map : (json -> 'a) -> obj -> (string * 'a) list
val check_unique_items : json -> 'a list -> unit
