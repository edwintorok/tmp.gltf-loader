open Format

module Identifier : sig
  type t
  module Capitalized : sig
    type t
    val v : string -> t
    val pp: formatter -> t -> unit
    val compare : t -> t -> int
  end
  val v : string -> t
  val prefix: Capitalized.t -> t -> t
  val pp: formatter -> t -> unit
end = struct
  type t = string

  let prefix pfx id =
    pfx ^ "." ^ id

  let v = function
    |"and"|"as"|"assert"|"asr"|"begin"|"class"
    |"constraint"|"do"|"done"|"downto"|"else"|"end"
    |"exception"|"external"|"false"|"for"|"fun"|"function"
    |"functor"|"if"|"in"|"include"|"inherit"|"initializer"
    |"land"|"lazy"|"let"|"lor"|"lsl"|"lsr"
    |"lxor"|"match"|"method"|"mod"|"module"|"mutable"
    |"new"|"object"|"of"|"open"|"or"|"private"
    |"rec"|"sig"|"struct"|"then"|"to"|"true"
    |"try"|"type"|"val"|"virtual"|"when"|"while"
    |"with"
    |"parser"|"value" as keyword ->
        keyword ^ "_"
    | s ->
      let buf = Buffer.create 128 in
      for i = 0 to String.length s - 1 do
        let c = s.[i] in
        if (c >= 'a' && c <= 'z') || c = '_' then
          Buffer.add_char buf c
        else if c >= 'A' && c <= 'Z' then begin
          if i > 0 then
            Buffer.add_char buf '_';
          Buffer.add_char buf (Char.lowercase c)
        end
        else if i > 0 && c >= '0' && c <= '9' then
          Buffer.add_char buf c
        else
          Buffer.add_char buf '_'
      done;
      Buffer.contents buf

  module Capitalized = struct
    type t = string
    let v name =
      let s = String.copy name in
      if String.length s = 0 then
        invalid_arg "module name cannot be empty";
      s.[0] <- Char.uppercase s.[0];
      if s.[0] < 'A' || s.[0] > 'Z' then
        invalid_arg "Bad volume name: must start with letter";
      for i = 1 to String.length s - 1 do
        let c = s.[i] in
        if not ((c >= 'a' && c <= 'z') || c = '_'
                || (c >= 'A' && c <= 'Z') ||
                (c >= '0' && c <= '9')) then
          s.[i] <- '_'
      done;
      s
    let pp = pp_print_string
    let compare = Pervasives.compare
  end
  let pp = pp_print_string
end

type type_expr =
  | Tname of Identifier.t
  | TString of string option (* of <default value> *)
  | TFloat of float option
  | TInt64 of int64 option
  | TBool of bool option
  | TArray of type_expr
  | TMap of type_expr
  | TOption of type_expr

let rec pp_type_expr ppf = function
  | Tname id -> Identifier.pp ppf id
  | TString _ -> pp_print_string ppf "string"
  | TFloat _ -> pp_print_string ppf "float"
  | TInt64 _ -> pp_print_string ppf "int64"
  | TBool _ -> pp_print_string ppf "bool"
  | TArray e ->
      fprintf ppf "%a array" pp_type_expr e
  | TMap e ->
      fprintf ppf "%a StringMap.t" pp_type_expr e
  | TOption e ->
      fprintf ppf "%a option" pp_type_expr e

type doc = string option
type type_decl =
  (* TODO: uncapitalized ident!!! *)
  | Record of (string * type_expr * doc) list
  | Expr of type_expr

type typedef = Identifier.t * type_decl

let pp_doc ppf doc = fprintf ppf "@[(** %s *)@]" doc

let pp_type_decl ppf = function
  | Record fields ->
      fprintf ppf "{@[<v 2>";
      List.iter (fun (field, expr, doc) ->
        fprintf ppf "@,%a: %a;" Identifier.pp (Identifier.v field) pp_type_expr expr;
        match doc with
        | None -> ()
        | Some doc ->
            fprintf ppf "@ %a" pp_doc doc
      ) fields;
      fprintf ppf "@]@,}"
  | Expr e ->
      pp_type_expr ppf e

let pp_type_decls ppf = function
  | [] -> ()
  | (name, decl) :: tl ->
      fprintf ppf "@[<v>type %a =@[@ %a@]@,"
        Identifier.pp name pp_type_decl decl;
      List.iter (fun (name, decl) ->
        fprintf ppf "and %a =@[@ %a@]@,"
          Identifier.pp name pp_type_decl decl
      ) tl;
      fprintf ppf "@]"

let pp_type_sig ppf (name, decl) doc =
  fprintf ppf "@[<v>";
  begin match doc with
  | None -> ()
  | Some doc -> fprintf ppf "%a@ " pp_doc doc
  end;
  fprintf ppf "type %a =@[@ %a@]@,"
    Identifier.pp name pp_type_decl decl;
  fprintf ppf "@]"

let pp_field_parser ppf = function
  | TString (Some default) -> fprintf ppf " ~default:%S get_string" default
  | TString None -> fprintf ppf "_required get_string"
  | TOption (TString None) -> fprintf ppf "_opt get_string"

  | TFloat (Some default) -> fprintf ppf " ~default:(%F) get_float" default
  | TFloat None -> fprintf ppf "_required get_float"
  | TOption (TFloat None) -> fprintf ppf "_opt get_float"

  | TInt64 (Some default) -> fprintf ppf " ~default:(%LdL) get_int64" default
  | TInt64 None -> fprintf ppf "_required get_int64"
  | TOption (TInt64 None) -> fprintf ppf "_opt get_int64"

  | TBool (Some default) -> fprintf ppf " ~default:%b get_boolean" default
  | TBool None -> fprintf ppf "_required get_boolean"
  | TOption (TBool None) -> fprintf ppf "_opt get_boolean"

  | TArray (TString None) -> fprintf ppf "_required (get_array get_string)"
  | TOption (TArray (TString None)) -> fprintf ppf "_opt (get_array get_string)"

  | TArray (TInt64 None) -> fprintf ppf "_required (get_array get_int64)"
  | TOption (TArray (TInt64 None)) -> fprintf ppf "_opt (get_array get_int64)"

  | TArray (TFloat None) -> fprintf ppf "_required (get_array get_float)"
  | TOption (TArray (TFloat None)) -> fprintf ppf "_opt (get_array get_float)"

  | TArray (TBool None) -> fprintf ppf "_required (get_array get_boolean)"
  | TOption (TArray (TBool None)) -> fprintf ppf "_opt (get_array get_boolean)"

  | TArray (Tname id) -> fprintf ppf "_required (get_array %a_of_json)"
    Identifier.pp id
  | TOption (TArray (Tname id)) -> fprintf ppf "_opt (get_array %a_of_json)"
    Identifier.pp id

  | Tname id when id = Identifier.v "json" -> fprintf ppf " ~default:`Null json_of_json"

  | Tname id ->  fprintf ppf "_required %a_of_json" Identifier.pp id
  | TOption (Tname id) ->  fprintf ppf "_opt %a_of_json" Identifier.pp id

  | TMap (Tname id) -> fprintf ppf "_dict %a_of_json" Identifier.pp id

  | TArray (TOption _) -> failwith "TODO: array opt codegen not implemented for this type"
  | TArray _ -> failwith "TODO: array codegen not implemented for this type"
  | TOption _ -> failwith "TODO: unsupported option codegen"
  | TMap _ -> failwith "TODO: unsupported map codegen"
(*  | _ -> failwith "TODO: codegen not implemented for this type"*)

let pp_parser ppf (name, decl) =
  fprintf ppf "@[<v 2>let %a_of_json json =@," Identifier.pp name;
  begin match decl with
  | Record fields ->
    fprintf ppf "let obj = get_dict json in@,{@[<v 2>";
    List.iter (fun (field, decl, _) ->
      fprintf ppf "@,%a = get_field%a obj %S;"
        Identifier.pp (Identifier.v field)
        pp_field_parser decl
        field
    ) fields;
    fprintf ppf "@]@,}"
  | Expr (TMap (TString None)) ->
      fprintf ppf "get_dict_map get_string json"
  | Expr _ ->
      failwith "TODO: unsupported codegen Expr"
  end;
  fprintf ppf "@]@,"

let pp_parser_sig ppf (name, decl) =
  fprintf ppf "@[val %a_of_json : json -> %a@]"
    Identifier.pp name Identifier.pp name
