(* Generated by json-schema. DO NOT EDIT. *)
open Jsonparsercommon
open Ezjsonm
(** *)
module GeographicLocation = struct
  type t =
          {
             longitude: float;
             (** The longitude of the asset as defined by the WGS84 world geodetic system in radians. Valid values range from -PI to PI radians. *)
             latitude: float;
             (** The latitude of the asset as defined by the WGS84 world geodetic system in radians. Valid values range from -PI/2 to PI/2 radians. *)
             altitude: float;
             (** The altitude of the asset. *)
             altitude_mode: string;
             (** Altitude follows the Keyhole Markup Language (KML) standard rather than the WGS84 calculation of height. That is, it can be relative to terrain, or relative to sea level. This property indicates whether the altitude value should be interpreted as the distance in meters from sea level, "absolute", or from the altitude of the terrain at the latitude/longitude point, "relativeToGround." *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       longitude = get_field_required get_float obj "longitude";
       latitude = get_field_required get_float obj "latitude";
       altitude = get_field ~default:(0.) get_float obj "altitude";
       altitude_mode = get_field ~default:"relativeToGround" get_string obj "altitudeMode";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Asset = struct
  type t =
          {
             copyright: string option;
             (** A copyright message suitable for display to credit the model author. *)
             geographic_location: GeographicLocation.t option;
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       copyright = get_field_opt get_string obj "copyright";
       geographic_location = get_field_opt GeographicLocation.t_of_json obj "geographicLocation";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module MeshAttribute = struct
  type t =
          {
             buffer_view: string;
             (** The id (JSON property name) of the bufferView referenced by this attribute. *)
             byte_offset: int64;
             (** The offset relative to the bufferView in bytes.  Similar to vertexAttribPointer() pointer (ES) / offset (WebGL) parameter. *)
             byte_stride: int64;
             (** The stride, in bytes, between attributes referenced by this accessor.  vertexAttribPointer() stride parameter. *)
             count: int64;
             (** The number of attributes referenced by this accessor, not to be confused with the number of bytes or number of components. *)
             type_: string;
             (** vertexAttribPointer() type and size parameters.  Corresponding typed arrays: Int8Array, Uint8Array, Int16Array, Uint16Array, and Float32Array. *)
             normalized: bool;
             (** vertexAttribPointer() normalized parameter. *)
             max: float array;
             (** Maximum value of each component in this attribute. *)
             min: float array;
             (** Minimum value of each component in this attribute. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       buffer_view = get_field_required get_string obj "bufferView";
       byte_offset = get_field_required get_int64 obj "byteOffset";
       byte_stride = get_field_required get_int64 obj "byteStride";
       count = get_field_required get_int64 obj "count";
       type_ = get_field_required get_string obj "type";
       normalized = get_field ~default:false get_boolean obj "normalized";
       max = get_field_required (get_array get_float) obj "max";
       min = get_field_required (get_array get_float) obj "min";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Indices = struct
  type t =
          {
             buffer_view: string;
             (** The id (JSON property name) of the bufferView to reference for index data. *)
             byte_offset: float;
             (** The offset relative to the bufferView in bytes. Similar to drawElements indices (ES) / offset (WebGL) parameter. *)
             count: float;
             (** The number of indices referenced, not to be confused with the number of bytes.  The drawElements count parameter. *)
             type_: string;
             (** drawElements() type parameter.  Corresponding typed array: Uint16Array. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       buffer_view = get_field_required get_string obj "bufferView";
       byte_offset = get_field_required get_float obj "byteOffset";
       count = get_field_required get_float obj "count";
       type_ = get_field_required get_string obj "type";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Buffer = struct
  type t =
          {
             path: string;
             (** The URL of the binary buffer.  Relative URLs are relative to the .json file that references the shader.  Instead of referencing an external shader, the URL can also be a base64 data URI. *)
             byte_length: float;
             (** The length of the buffer in bytes. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       path = get_field_required get_string obj "path";
       byte_length = get_field_required get_float obj "byteLength";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module BufferView = struct
  type t =
          {
             buffer: string;
             (** The id (JSON property name) of the buffer referenced by this view. *)
             byte_offset: float;
             (** The offset into the buffer, in bytes. *)
             byte_length: float;
             (** The length of the buffer view, in bytes. *)
             target: int64;
             (** Required target type for bindBuffer(), must be ARRAY_BUFFER or ELEMENT_ARRAY_BUFFER *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       buffer = get_field_required get_string obj "buffer";
       byte_offset = get_field ~default:(0.) get_float obj "byteOffset";
       byte_length = get_field_required get_float obj "byteLength";
       target = get_field_required get_int64 obj "target";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Orthographic = struct
  type t =
          {
             xmag: float;
             (** The floating-point horizontal magnification of the view. *)
             ymag: float;
             (** The floating-point vertical magnification of the view. *)
             zfar: float option;
             (** The floating-point distance to the far clipping plane. *)
             znear: float option;
             (** The floating-point distance to the near clipping plane. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       xmag = get_field ~default:(1.) get_float obj "xmag";
       ymag = get_field ~default:(1.) get_float obj "ymag";
       zfar = get_field_opt get_float obj "zfar";
       znear = get_field_opt get_float obj "znear";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Perspective = struct
  type t =
          {
             aspect_ratio: float option;
             (** The floating-point aspect ratio of the field of view. *)
             yfov: float;
             (** The floating-point vertical field of view in radians. *)
             zfar: float option;
             (** The floating-point distance to the far clipping plane. *)
             znear: float option;
             (** The floating-point distance to the near clipping plane. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       aspect_ratio = get_field_opt get_float obj "aspect_ratio";
       yfov = get_field_required get_float obj "yfov";
       zfar = get_field_opt get_float obj "zfar";
       znear = get_field_opt get_float obj "znear";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Camera = struct
  type t =
          {
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             orthographic: Orthographic.t option;
             perspective: Perspective.t option;
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       name = get_field_opt get_string obj "name";
       orthographic = get_field_opt Orthographic.t_of_json obj "orthographic";
       perspective = get_field_opt Perspective.t_of_json obj "perspective";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Image = struct
  type t =
          {
             path: string;
             (** The URL of the image.  Relative URLs are relative to the .json file that references the image.  Instead of referencing an external image, the URL can also be a data URI. *)
             generate_mipmap: bool;
             (** generateMipmap() *)
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       path = get_field_required get_string obj "path";
       generate_mipmap = get_field ~default:true get_boolean obj "generateMipmap";
       name = get_field_opt get_string obj "name";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Parameters = struct
  type t = {
              extra: json;
              (** Optional application-specific data. *)}
  let t_of_json json =
    let obj = get_dict json in
    {
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

type instance_technique =
                         {
                            technique: string;
                            (** The id (JSON property name) of the default technique referenced by this material. *)
                            values: json;
                         }
let instance_technique_of_json json =
  let obj = get_dict json in
  {
     technique = get_field_required get_string obj "technique";
     values = get_field ~default:`Null json_of_json obj "values";
  }


type instance_program = {
                           program: string;}
let instance_program_of_json json =
  let obj = get_dict json in
  {
     program = get_field_required get_string obj "program";
  }


type blend_equation_separate =
                              {
                                 rgb: string;
                                 (** blendEquationSeparate() *)
                                 alpha: string;
                                 (** blendEquationSeparate() *)
                              }
let blend_equation_separate_of_json json =
  let obj = get_dict json in
  {
     rgb = get_field ~default:"FUNC_ADD" get_string obj "rgb";
     alpha = get_field ~default:"FUNC_ADD" get_string obj "alpha";
  }


type blend_func =
                 {
                    sfactor: int64;
                    (** blendFunc() *)
                    dfactor: int64;
                    (** blendFunc() *)
                 }
let blend_func_of_json json =
  let obj = get_dict json in
  {
     sfactor = get_field ~default:(1L) get_int64 obj "sfactor";
     dfactor = get_field ~default:(0L) get_int64 obj "dfactor";
  }


type blend_func_separate =
                          {
                             src_r_g_b: string;
                             (** blendFuncSeparate() *)
                             src_alpha: string;
                             (** blendFuncSeparate() *)
                             dst_r_g_b: string;
                             (** blendFuncSeparate() *)
                             dst_alpha: string;
                             (** blendFuncSeparate() *)
                          }
let blend_func_separate_of_json json =
  let obj = get_dict json in
  {
     src_r_g_b = get_field ~default:"ONE" get_string obj "srcRGB";
     src_alpha = get_field ~default:"ONE" get_string obj "srcAlpha";
     dst_r_g_b = get_field ~default:"ZERO" get_string obj "dstRGB";
     dst_alpha = get_field ~default:"ZERO" get_string obj "dstAlpha";
  }


type color_mask =
                 {
                    red: bool;
                    (** colorMask() *)
                    green: bool;
                    (** colorMask() *)
                    blue: bool;
                    (** colorMask() *)
                    alpha: bool;
                    (** colorMask() *)
                 }
let color_mask_of_json json =
  let obj = get_dict json in
  {
     red = get_field ~default:true get_boolean obj "red";
     green = get_field ~default:true get_boolean obj "green";
     blue = get_field ~default:true get_boolean obj "blue";
     alpha = get_field ~default:true get_boolean obj "alpha";
  }


type depth_range =
                  {
                     z_near: float;
                     (** depthRange() *)
                     z_far: float;
                     (** depthRange() *)
                  }
let depth_range_of_json json =
  let obj = get_dict json in
  {
     z_near = get_field ~default:(0.) get_float obj "zNear";
     z_far = get_field ~default:(1.) get_float obj "zFar";
  }


type polygon_offset =
                     {
                        factor: float;
                        (** polygonOffset() *)
                        units: float;
                        (** polygonOffset() *)
                     }
let polygon_offset_of_json json =
  let obj = get_dict json in
  {
     factor = get_field ~default:(0.) get_float obj "factor";
     units = get_field ~default:(0.) get_float obj "units";
  }


type sample_coverage =
                      {
                         value_: float;
                         (** sampleCoverage() *)
                         invert: bool;
                         (** sampleCoverage() *)
                      }
let sample_coverage_of_json json =
  let obj = get_dict json in
  {
     value_ = get_field ~default:(1.) get_float obj "value";
     invert = get_field ~default:false get_boolean obj "invert";
  }


type scissor =
              {
                 x: int64;
                 (** scissor() *)
                 y: int64;
                 (** scissor() *)
                 width: int64;
                 (** scissor() *)
                 height: int64;
                 (** scissor() *)
              }
let scissor_of_json json =
  let obj = get_dict json in
  {
     x = get_field ~default:(0L) get_int64 obj "x";
     y = get_field ~default:(0L) get_int64 obj "y";
     width = get_field ~default:(0L) get_int64 obj "width";
     height = get_field ~default:(0L) get_int64 obj "height";
  }


type stencil_func =
                   {
                      func: string;
                      (** stencilFunc() *)
                      ref: int64;
                      (** stencilFunc() *)
                      mask: int64 option;
                      (** stencilFunc().  The default is all ones. *)
                   }
let stencil_func_of_json json =
  let obj = get_dict json in
  {
     func = get_field ~default:"ALWAYS" get_string obj "func";
     ref = get_field ~default:(0L) get_int64 obj "ref";
     mask = get_field_opt get_int64 obj "mask";
  }


type stencil_func_separate =
                            {
                               front: string;
                               (** stencilFuncSeparate() *)
                               back: string;
                               (** stencilFuncSeparate() *)
                               ref: int64;
                               (** stencilFuncSeparate() *)
                               mask: int64 option;
                               (** stencilFuncSeparate().  The default is all ones. *)
                            }
let stencil_func_separate_of_json json =
  let obj = get_dict json in
  {
     front = get_field ~default:"ALWAYS" get_string obj "front";
     back = get_field ~default:"ALWAYS" get_string obj "back";
     ref = get_field ~default:(0L) get_int64 obj "ref";
     mask = get_field_opt get_int64 obj "mask";
  }


type stencil_op =
                 {
                    fail: string;
                    (** stencilOp() *)
                    zfail: string;
                    (** stencilOp() *)
                    zpass: string;
                    (** stencilOp() *)
                 }
let stencil_op_of_json json =
  let obj = get_dict json in
  {
     fail = get_field ~default:"KEEP" get_string obj "fail";
     zfail = get_field ~default:"KEEP" get_string obj "zfail";
     zpass = get_field ~default:"KEEP" get_string obj "zpass";
  }


type stencil_op_separate =
                          {
                             face: string;
                             (** stencilOpSeparate() *)
                             fail: string;
                             (** stencilOpSeparate() *)
                             zfail: string;
                             (** stencilOpSeparate() *)
                             zpass: string;
                             (** stencilOpSeparate() *)
                          }
let stencil_op_separate_of_json json =
  let obj = get_dict json in
  {
     face = get_field ~default:"FRONT_AND_BACK" get_string obj "face";
     fail = get_field ~default:"KEEP" get_string obj "fail";
     zfail = get_field ~default:"KEEP" get_string obj "zfail";
     zpass = get_field ~default:"KEEP" get_string obj "zpass";
  }


module States = struct
  type t =
          {
             blend_enable: bool;
             (** enable(BLEND) or disable(BLEND) *)
             blend_equation: int64;
             (** blendEquation() *)
             blend_equation_separate: blend_equation_separate option;
             (** blendEquationSeparate() *)
             blend_func: blend_func option;
             (** blendFunc() *)
             blend_func_separate: blend_func_separate option;
             (** blendFuncSeparate() *)
             color_mask: color_mask option;
             (** colorMask() *)
             cull_face: string;
             (** cullFace() *)
             cull_face_enable: bool;
             (** enable(CULL_FACE) or disable(CULL_FACE) *)
             depth_func: string;
             (** depthFunc() *)
             depth_mask: bool;
             (** depthMask() *)
             depth_range: depth_range option;
             (** depthRange() *)
             depth_test_enable: bool;
             (** enable(DEPTH_TEST) or disable(DEPTH_TEST) *)
             dither_enable: bool;
             (** enable(DITHER) or disable(DITHER) *)
             front_face: string;
             (** frontFace() *)
             line_width: float;
             (** lineWidth() *)
             point_size: float option;
             (** gl_PointSize *)
             polygon_offset: polygon_offset option;
             (** polygonOffset() *)
             polygon_offset_fill_enable: bool;
             (** enable(POLYGON_OFFSET_FILL) or disable(POLYGON_OFFSET_FILL) *)
             sample_alpha_to_coverage_enable: bool;
             (** enable(SAMPLE_ALPHA_TO_COVERAGE) or disable(SAMPLE_ALPHA_TO_COVERAGE) *)
             sample_coverage: sample_coverage option;
             (** sampleCoverage() *)
             sample_coverage_enable: bool;
             (** enable(SAMPLE_COVERAGE) or disable(SAMPLE_COVERAGE) *)
             scissor: scissor option;
             (** scissor() *)
             scissor_test_enable: bool;
             (** enable(SCISSOR_TEST) or disable(SCISSOR_TEST) *)
             stencil_func: stencil_func option;
             (** stencilFunc() *)
             stencil_func_separate: stencil_func_separate option;
             (** stencilFuncSeparate() *)
             stencil_mask: int64 option;
             (** stencilMask().  The default is all ones. *)
             stencil_op: stencil_op option;
             (** stencilOp() *)
             stencil_op_separate: stencil_op_separate option;
             (** stencilOpSeparate() *)
             stencil_test_enable: bool;
             (** enable(STENCIL_TEST) or disable(STENCIL_TEST) *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       blend_enable = get_field ~default:false get_boolean obj "blendEnable";
       blend_equation = get_field ~default:(32774L) get_int64 obj "blendEquation";
       blend_equation_separate = get_field_opt blend_equation_separate_of_json obj "blendEquationSeparate";
       blend_func = get_field_opt blend_func_of_json obj "blendFunc";
       blend_func_separate = get_field_opt blend_func_separate_of_json obj "blendFuncSeparate";
       color_mask = get_field_opt color_mask_of_json obj "colorMask";
       cull_face = get_field ~default:"BACK" get_string obj "cullFace";
       cull_face_enable = get_field ~default:false get_boolean obj "cullFaceEnable";
       depth_func = get_field ~default:"LESS" get_string obj "depthFunc";
       depth_mask = get_field ~default:true get_boolean obj "depthMask";
       depth_range = get_field_opt depth_range_of_json obj "depthRange";
       depth_test_enable = get_field ~default:false get_boolean obj "depthTestEnable";
       dither_enable = get_field ~default:false get_boolean obj "ditherEnable";
       front_face = get_field ~default:"CCW" get_string obj "frontFace";
       line_width = get_field ~default:(1.) get_float obj "lineWidth";
       point_size = get_field_opt get_float obj "pointSize";
       polygon_offset = get_field_opt polygon_offset_of_json obj "polygonOffset";
       polygon_offset_fill_enable = get_field ~default:false get_boolean obj "polygonOffsetFillEnable";
       sample_alpha_to_coverage_enable = get_field ~default:false get_boolean obj "sampleAlphaToCoverageEnable";
       sample_coverage = get_field_opt sample_coverage_of_json obj "sampleCoverage";
       sample_coverage_enable = get_field ~default:false get_boolean obj "sampleCoverageEnable";
       scissor = get_field_opt scissor_of_json obj "scissor";
       scissor_test_enable = get_field ~default:false get_boolean obj "scissorTestEnable";
       stencil_func = get_field_opt stencil_func_of_json obj "stencilFunc";
       stencil_func_separate = get_field_opt stencil_func_separate_of_json obj "stencilFuncSeparate";
       stencil_mask = get_field_opt get_int64 obj "stencilMask";
       stencil_op = get_field_opt stencil_op_of_json obj "stencilOp";
       stencil_op_separate = get_field_opt stencil_op_separate_of_json obj "stencilOpSeparate";
       stencil_test_enable = get_field ~default:false get_boolean obj "stencilTestEnable";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Pass = struct
  type t =
          {
             instance_program: instance_program option;
             states: States.t option;
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       instance_program = get_field_opt instance_program_of_json obj "instanceProgram";
       states = get_field_opt States.t_of_json obj "states";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Technique = struct
  type t =
          {
             parameters: Parameters.t option;
             pass: string;
             passes: Pass.t StringMap.t;
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       parameters = get_field_opt Parameters.t_of_json obj "parameters";
       pass = get_field_required get_string obj "pass";
       passes = get_field_dict Pass.t_of_json obj "passes";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Material = struct
  type t =
          {
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             parameters: Parameters.t option;
             instance_technique: instance_technique option;
             techniques: Technique.t StringMap.t;
             (** The techniques available to this material. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       name = get_field_opt get_string obj "name";
       parameters = get_field_opt Parameters.t_of_json obj "parameters";
       instance_technique = get_field_opt instance_technique_of_json obj "instanceTechnique";
       techniques = get_field_dict Technique.t_of_json obj "techniques";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Semantics = struct
  type t = string StringMap.t
  let t_of_json json =
    get_dict_map get_string json
  
end

module Primitive = struct
  type t =
          {
             indices: string;
             material: string;
             (** Specifies the material to apply to this primitive when rendering. *)
             primitive: int64;
             (** Specifies what kind of primitives to render. *)
             semantics: Semantics.t option;
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       indices = get_field_required get_string obj "indices";
       material = get_field_required get_string obj "material";
       primitive = get_field_required get_int64 obj "primitive";
       semantics = get_field_opt Semantics.t_of_json obj "semantics";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Mesh = struct
  type t =
          {
             attributes: MeshAttribute.t StringMap.t;
             (** TODO *)
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             primitives: Primitive.t array option;
             (** Primitives that form the geometry in this mesh. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       attributes = get_field_dict MeshAttribute.t_of_json obj "attributes";
       name = get_field_opt get_string obj "name";
       primitives = get_field_opt (get_array Primitive.t_of_json) obj "primitives";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Node = struct
  type t =
          {
             children: string array option;
             (** The node's children.  Each string in the array must match an existing id (JSON property name) in this model's nodes.  Children are affected by the node's matrix. *)
             matrix: float array option;
             (** A floating-point 4x4 transformation matrix stored in column-major order.  It is directly usable by uniformMatrix4fv with transpose equal to false. *)
             meshes: string array option;
             (** The meshes that are part of this node.  Each string in the array must match an existing id (JSON property name) in this model's meshes.  Multiple meshes are allowed so each can share the same transform matrix. *)
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       children = get_field_opt (get_array get_string) obj "children";
       matrix = get_field_opt (get_array get_float) obj "matrix";
       meshes = get_field_opt (get_array get_string) obj "meshes";
       name = get_field_opt get_string obj "name";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Shader = struct
  type t =
          {
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             path: string;
             (** The URL of the GLSL source.  Relative URLs are relative to the .json file that references the shader.  Instead of referencing an external shader, the URL can also be a text/plain data URI. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       name = get_field_opt get_string obj "name";
       path = get_field_required get_string obj "path";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Uniform = struct
  type t =
          {
             semantic: string option;
             (** TODO *)
             symbol: string;
             (** TODO *)
             type_: string;
             (** TODO *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       semantic = get_field_opt get_string obj "semantic";
       symbol = get_field_required get_string obj "symbol";
       type_ = get_field_required get_string obj "type";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

module Program = struct
  type t =
          {
             attributes: string array;
             (** TODO *)
             fragment_shader: string;
             (** The id (JSON property name) of the fragment shader referenced by this program. *)
             uniforms: Uniform.t array option;
             (** TODO *)
             vertex_shader: string;
             (** The id (JSON property name) of the vertex shader referenced by this program. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  let t_of_json json =
    let obj = get_dict json in
    {
       attributes = get_field_required (get_array get_string) obj "attributes";
       fragment_shader = get_field_required get_string obj "fragmentShader";
       uniforms = get_field_opt (get_array Uniform.t_of_json) obj "uniforms";
       vertex_shader = get_field_required get_string obj "vertexShader";
       extra = get_field ~default:`Null json_of_json obj "extra";
    }
  
end

type t =
        {
           asset: Asset.t option;
           accessors: json StringMap.t;
           buffers: Buffer.t StringMap.t;
           (** The buffers used in this model. *)
           buffer_views: BufferView.t StringMap.t;
           (** The buffer views used in this model. *)
           cameras: Camera.t StringMap.t;
           (** The cameras available to this model. *)
           images: Image.t StringMap.t;
           (** The images used in this model. *)
           materials: Material.t StringMap.t;
           (** The materials used in this model. *)
           meshes: Mesh.t StringMap.t;
           (** The meshes that make up this model. *)
           nodes: Node.t StringMap.t;
           (** The nodes that make up this model. *)
           shaders: Shader.t StringMap.t;
           (** The shaders used in this model. *)
           techniques: Technique.t StringMap.t;
           (** The techniques used in this model. *)
           version: string option;
           (** The glTF version. *)
           profile: string;
           (** The glTF profile used for this asset. *)
           programs: Program.t StringMap.t;
           extra: json;
           (** Optional application-specific data. *)
        }
let t_of_json json =
  let obj = get_dict json in
  {
     asset = get_field_opt Asset.t_of_json obj "asset";
     accessors = get_field_dict json_of_json obj "accessors";
     buffers = get_field_dict Buffer.t_of_json obj "buffers";
     buffer_views = get_field_dict BufferView.t_of_json obj "bufferViews";
     cameras = get_field_dict Camera.t_of_json obj "cameras";
     images = get_field_dict Image.t_of_json obj "images";
     materials = get_field_dict Material.t_of_json obj "materials";
     meshes = get_field_dict Mesh.t_of_json obj "meshes";
     nodes = get_field_dict Node.t_of_json obj "nodes";
     shaders = get_field_dict Shader.t_of_json obj "shaders";
     techniques = get_field_dict Technique.t_of_json obj "techniques";
     version = get_field_opt get_string obj "version";
     profile = get_field_required get_string obj "profile";
     programs = get_field_dict Program.t_of_json obj "programs";
     extra = get_field ~default:`Null json_of_json obj "extra";
  }

