(* Generated by json-schema. DO NOT EDIT. *)
open Jsonparsercommon
(** *)
module GeographicLocation : sig
  (** Geographic location of the asset. *)
  type t =
          {
             longitude: float;
             (** The longitude of the asset as defined by the WGS84 world geodetic system in radians. Valid values range from -PI to PI radians. *)
             latitude: float;
             (** The latitude of the asset as defined by the WGS84 world geodetic system in radians. Valid values range from -PI/2 to PI/2 radians. *)
             altitude: float;
             (** The altitude of the asset. *)
             altitude_mode: string;
             (** Altitude follows the Keyhole Markup Language (KML) standard rather than the WGS84 calculation of height. That is, it can be relative to terrain, or relative to sea level. This property indicates whether the altitude value should be interpreted as the distance in meters from sea level, "absolute", or from the altitude of the terrain at the latitude/longitude point, "relativeToGround." *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Asset : sig
  (** Asset-management information for this model. *)
  type t =
          {
             copyright: string option;
             (** A copyright message suitable for display to credit the model author. *)
             geographic_location: GeographicLocation.t option;
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module MeshAttribute : sig
  (** compliant with vertexAttribPointer() API *)
  type t =
          {
             buffer_view: string;
             (** The id (JSON property name) of the bufferView referenced by this attribute. *)
             byte_offset: int64;
             (** The offset relative to the bufferView in bytes.  Similar to vertexAttribPointer() pointer (ES) / offset (WebGL) parameter. *)
             byte_stride: int64;
             (** The stride, in bytes, between attributes referenced by this accessor.  vertexAttribPointer() stride parameter. *)
             count: int64;
             (** The number of attributes referenced by this accessor, not to be confused with the number of bytes or number of components. *)
             type_: string;
             (** vertexAttribPointer() type and size parameters.  Corresponding typed arrays: Int8Array, Uint8Array, Int16Array, Uint16Array, and Float32Array. *)
             normalized: bool;
             (** vertexAttribPointer() normalized parameter. *)
             max: float array;
             (** Maximum value of each component in this attribute. *)
             min: float array;
             (** Minimum value of each component in this attribute. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Indices : sig
  (** drawElements() *)
  type t =
          {
             buffer_view: string;
             (** The id (JSON property name) of the bufferView to reference for index data. *)
             byte_offset: float;
             (** The offset relative to the bufferView in bytes. Similar to drawElements indices (ES) / offset (WebGL) parameter. *)
             count: float;
             (** The number of indices referenced, not to be confused with the number of bytes.  The drawElements count parameter. *)
             type_: string;
             (** drawElements() type parameter.  Corresponding typed array: Uint16Array. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Buffer : sig
  (** A buffer with vertex or index data. *)
  type t =
          {
             path: string;
             (** The URL of the binary buffer.  Relative URLs are relative to the .json file that references the shader.  Instead of referencing an external shader, the URL can also be a base64 data URI. *)
             byte_length: float;
             (** The length of the buffer in bytes. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module BufferView : sig
  (** Represents a view of a buffer for indexing and manipulation *)
  type t =
          {
             buffer: string;
             (** The id (JSON property name) of the buffer referenced by this view. *)
             byte_offset: float;
             (** The offset into the buffer, in bytes. *)
             byte_length: float;
             (** The length of the buffer view, in bytes. *)
             target: int64;
             (** Required target type for bindBuffer(), must be ARRAY_BUFFER or ELEMENT_ARRAY_BUFFER *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Orthographic : sig
  (** An orthographic camera.  These properties are used to create an orthographic projection matrix. *)
  type t =
          {
             xmag: float;
             (** The floating-point horizontal magnification of the view. *)
             ymag: float;
             (** The floating-point vertical magnification of the view. *)
             zfar: float option;
             (** The floating-point distance to the far clipping plane. *)
             znear: float option;
             (** The floating-point distance to the near clipping plane. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Perspective : sig
  (** A perspective camera.  These properties are used to create a perspective projection matrix. *)
  type t =
          {
             aspect_ratio: float option;
             (** The floating-point aspect ratio of the field of view. *)
             yfov: float;
             (** The floating-point vertical field of view in radians. *)
             zfar: float option;
             (** The floating-point distance to the far clipping plane. *)
             znear: float option;
             (** The floating-point distance to the near clipping plane. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Camera : sig
  (** A view into the scene.  The camera describes the projection, but not the camera's position and direction. *)
  type t =
          {
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             orthographic: Orthographic.t option;
             perspective: Perspective.t option;
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Image : sig
  (** An image. *)
  type t =
          {
             path: string;
             (** The URL of the image.  Relative URLs are relative to the .json file that references the image.  Instead of referencing an external image, the URL can also be a data URI. *)
             generate_mipmap: bool;
             (** generateMipmap() *)
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Parameters : sig
  type t = {
              extra: json;
              (** Optional application-specific data. *)}
  val t_of_json : json -> t
end

type instance_technique =
                         {
                            technique: string;
                            (** The id (JSON property name) of the default technique referenced by this material. *)
                            values: json;
                         }
val instance_technique_of_json : json -> instance_technique

type instance_program = {
                           program: string;}
val instance_program_of_json : json -> instance_program

(** blendEquationSeparate() *)
type blend_equation_separate =
                              {
                                 rgb: string;
                                 (** blendEquationSeparate() *)
                                 alpha: string;
                                 (** blendEquationSeparate() *)
                              }
val blend_equation_separate_of_json : json -> blend_equation_separate

(** blendFunc() *)
type blend_func =
                 {
                    sfactor: int64;
                    (** blendFunc() *)
                    dfactor: int64;
                    (** blendFunc() *)
                 }
val blend_func_of_json : json -> blend_func

(** blendFuncSeparate() *)
type blend_func_separate =
                          {
                             src_r_g_b: string;
                             (** blendFuncSeparate() *)
                             src_alpha: string;
                             (** blendFuncSeparate() *)
                             dst_r_g_b: string;
                             (** blendFuncSeparate() *)
                             dst_alpha: string;
                             (** blendFuncSeparate() *)
                          }
val blend_func_separate_of_json : json -> blend_func_separate

(** colorMask() *)
type color_mask =
                 {
                    red: bool;
                    (** colorMask() *)
                    green: bool;
                    (** colorMask() *)
                    blue: bool;
                    (** colorMask() *)
                    alpha: bool;
                    (** colorMask() *)
                 }
val color_mask_of_json : json -> color_mask

(** depthRange() *)
type depth_range =
                  {
                     z_near: float;
                     (** depthRange() *)
                     z_far: float;
                     (** depthRange() *)
                  }
val depth_range_of_json : json -> depth_range

(** polygonOffset() *)
type polygon_offset =
                     {
                        factor: float;
                        (** polygonOffset() *)
                        units: float;
                        (** polygonOffset() *)
                     }
val polygon_offset_of_json : json -> polygon_offset

(** sampleCoverage() *)
type sample_coverage =
                      {
                         value_: float;
                         (** sampleCoverage() *)
                         invert: bool;
                         (** sampleCoverage() *)
                      }
val sample_coverage_of_json : json -> sample_coverage

(** scissor() *)
type scissor =
              {
                 x: int64;
                 (** scissor() *)
                 y: int64;
                 (** scissor() *)
                 width: int64;
                 (** scissor() *)
                 height: int64;
                 (** scissor() *)
              }
val scissor_of_json : json -> scissor

(** stencilFunc() *)
type stencil_func =
                   {
                      func: string;
                      (** stencilFunc() *)
                      ref: int64;
                      (** stencilFunc() *)
                      mask: int64 option;
                      (** stencilFunc().  The default is all ones. *)
                   }
val stencil_func_of_json : json -> stencil_func

(** stencilFuncSeparate() *)
type stencil_func_separate =
                            {
                               front: string;
                               (** stencilFuncSeparate() *)
                               back: string;
                               (** stencilFuncSeparate() *)
                               ref: int64;
                               (** stencilFuncSeparate() *)
                               mask: int64 option;
                               (** stencilFuncSeparate().  The default is all ones. *)
                            }
val stencil_func_separate_of_json : json -> stencil_func_separate

(** stencilOp() *)
type stencil_op =
                 {
                    fail: string;
                    (** stencilOp() *)
                    zfail: string;
                    (** stencilOp() *)
                    zpass: string;
                    (** stencilOp() *)
                 }
val stencil_op_of_json : json -> stencil_op

(** stencilOpSeparate() *)
type stencil_op_separate =
                          {
                             face: string;
                             (** stencilOpSeparate() *)
                             fail: string;
                             (** stencilOpSeparate() *)
                             zfail: string;
                             (** stencilOpSeparate() *)
                             zpass: string;
                             (** stencilOpSeparate() *)
                          }
val stencil_op_separate_of_json : json -> stencil_op_separate

module States : sig
  (** Contains all rendering states to set up for the parent pass. *)
  type t =
          {
             blend_enable: bool;
             (** enable(BLEND) or disable(BLEND) *)
             blend_equation: int64;
             (** blendEquation() *)
             blend_equation_separate: blend_equation_separate option;
             (** blendEquationSeparate() *)
             blend_func: blend_func option;
             (** blendFunc() *)
             blend_func_separate: blend_func_separate option;
             (** blendFuncSeparate() *)
             color_mask: color_mask option;
             (** colorMask() *)
             cull_face: string;
             (** cullFace() *)
             cull_face_enable: bool;
             (** enable(CULL_FACE) or disable(CULL_FACE) *)
             depth_func: string;
             (** depthFunc() *)
             depth_mask: bool;
             (** depthMask() *)
             depth_range: depth_range option;
             (** depthRange() *)
             depth_test_enable: bool;
             (** enable(DEPTH_TEST) or disable(DEPTH_TEST) *)
             dither_enable: bool;
             (** enable(DITHER) or disable(DITHER) *)
             front_face: string;
             (** frontFace() *)
             line_width: float;
             (** lineWidth() *)
             point_size: float option;
             (** gl_PointSize *)
             polygon_offset: polygon_offset option;
             (** polygonOffset() *)
             polygon_offset_fill_enable: bool;
             (** enable(POLYGON_OFFSET_FILL) or disable(POLYGON_OFFSET_FILL) *)
             sample_alpha_to_coverage_enable: bool;
             (** enable(SAMPLE_ALPHA_TO_COVERAGE) or disable(SAMPLE_ALPHA_TO_COVERAGE) *)
             sample_coverage: sample_coverage option;
             (** sampleCoverage() *)
             sample_coverage_enable: bool;
             (** enable(SAMPLE_COVERAGE) or disable(SAMPLE_COVERAGE) *)
             scissor: scissor option;
             (** scissor() *)
             scissor_test_enable: bool;
             (** enable(SCISSOR_TEST) or disable(SCISSOR_TEST) *)
             stencil_func: stencil_func option;
             (** stencilFunc() *)
             stencil_func_separate: stencil_func_separate option;
             (** stencilFuncSeparate() *)
             stencil_mask: int64 option;
             (** stencilMask().  The default is all ones. *)
             stencil_op: stencil_op option;
             (** stencilOp() *)
             stencil_op_separate: stencil_op_separate option;
             (** stencilOpSeparate() *)
             stencil_test_enable: bool;
             (** enable(STENCIL_TEST) or disable(STENCIL_TEST) *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Pass : sig
  (** TODO *)
  type t =
          {
             instance_program: instance_program option;
             states: States.t option;
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Technique : sig
  (** TODO *)
  type t =
          {
             parameters: Parameters.t option;
             pass: string;
             passes: Pass.t StringMap.t;
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Material : sig
  (** TODO *)
  type t =
          {
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             parameters: Parameters.t option;
             instance_technique: instance_technique option;
             techniques: Technique.t StringMap.t;
             (** The techniques available to this material. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Semantics : sig
  (** TODO *)
  type t = string StringMap.t
  val t_of_json : json -> t
end

module Primitive : sig
  (** TODO *)
  type t =
          {
             indices: string;
             material: string;
             (** Specifies the material to apply to this primitive when rendering. *)
             primitive: int64;
             (** Specifies what kind of primitives to render. *)
             semantics: Semantics.t option;
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Mesh : sig
  (** TODO *)
  type t =
          {
             attributes: MeshAttribute.t StringMap.t;
             (** TODO *)
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             primitives: Primitive.t array option;
             (** Primitives that form the geometry in this mesh. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Node : sig
  (** A node in the model's node hierarchy.  Node can reference meshes, cameras, or lights. *)
  type t =
          {
             children: string array option;
             (** The node's children.  Each string in the array must match an existing id (JSON property name) in this model's nodes.  Children are affected by the node's matrix. *)
             matrix: float array option;
             (** A floating-point 4x4 transformation matrix stored in column-major order.  It is directly usable by uniformMatrix4fv with transpose equal to false. *)
             meshes: string array option;
             (** The meshes that are part of this node.  Each string in the array must match an existing id (JSON property name) in this model's meshes.  Multiple meshes are allowed so each can share the same transform matrix. *)
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Shader : sig
  (** A vertex or fragment shader. *)
  type t =
          {
             name: string option;
             (** The application-visible globally-unique name of this node. *)
             path: string;
             (** The URL of the GLSL source.  Relative URLs are relative to the .json file that references the shader.  Instead of referencing an external shader, the URL can also be a text/plain data URI. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Uniform : sig
  (** TODO *)
  type t =
          {
             semantic: string option;
             (** TODO *)
             symbol: string;
             (** TODO *)
             type_: string;
             (** TODO *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

module Program : sig
  (** TODO *)
  type t =
          {
             attributes: string array;
             (** TODO *)
             fragment_shader: string;
             (** The id (JSON property name) of the fragment shader referenced by this program. *)
             uniforms: Uniform.t array option;
             (** TODO *)
             vertex_shader: string;
             (** The id (JSON property name) of the vertex shader referenced by this program. *)
             extra: json;
             (** Optional application-specific data. *)
          }
  val t_of_json : json -> t
end

(** The glTF model. *)
type t =
        {
           asset: Asset.t option;
           accessors: json StringMap.t;
           buffers: Buffer.t StringMap.t;
           (** The buffers used in this model. *)
           buffer_views: BufferView.t StringMap.t;
           (** The buffer views used in this model. *)
           cameras: Camera.t StringMap.t;
           (** The cameras available to this model. *)
           images: Image.t StringMap.t;
           (** The images used in this model. *)
           materials: Material.t StringMap.t;
           (** The materials used in this model. *)
           meshes: Mesh.t StringMap.t;
           (** The meshes that make up this model. *)
           nodes: Node.t StringMap.t;
           (** The nodes that make up this model. *)
           shaders: Shader.t StringMap.t;
           (** The shaders used in this model. *)
           techniques: Technique.t StringMap.t;
           (** The techniques used in this model. *)
           version: string option;
           (** The glTF version. *)
           profile: string;
           (** The glTF profile used for this asset. *)
           programs: Program.t StringMap.t;
           extra: json;
           (** Optional application-specific data. *)
        }
val t_of_json : json -> t
