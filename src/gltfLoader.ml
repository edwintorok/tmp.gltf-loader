open Ezjsonm
open WrapGL
open GlTF
(* all data is little-endian *)
let () = assert (not Sys.big_endian)
module type IO = sig
  type uri
  val uri_of_string: ?parent:uri -> string -> uri
  val with_uri_contents: uri -> (string -> 'a) -> 'a
end

let bigarray_create k len = Bigarray.(Array1.create k c_layout len)
(*let int_value =
  let a = bigarray_create Bigarray.int32 1 in
  fun f -> f a; Int32.to_int a.{0}*)

(*let set_int =
  let a = bigarray_create Bigarray.int32 1 in
  fun f i -> a.{0} <- Int32.of_int i; f a; ()*)

module StringMap = Map.Make(String)
module Make(M:IO) = struct
  (*
  let parse_target = function
    | `Float f ->
        if int_of_float f = Gl.array_buffer then
          Gl.array_buffer
        else if int_of_float f = Gl.element_array_buffer then
          Gl.element_array_buffer
        else
          invalid_arg "target"
    | `String "ARRAY_BUFFER" ->
        Gl.array_buffer
    | `String "ELEMENT_ARRAY_BUFFER" ->
        Gl.element_array_buffer
    | _ -> invalid_arg "target"

  let bind_buffer ?target json buffers name =
    let buffer_view = find json ["bufferViews"; name] in
    let bufid = int_value (Gl.gen_buffers 1) in
    let buffer = get_string (find buffer_view ["buffer"]) in
    let byte_offset = get_int (find buffer_view ["byteOffset"]) in
    (*  default: 0 *)
    let byte_length = get_int (find buffer_view ["byteLength"]) in
    let target = match target with
     | None -> parse_target (find buffer_view ["target"])
     | Some t -> t in
    let buf = StringMap.find buffer buffers in
    let data = Bigarray.Array1.sub buf byte_offset byte_length in
    Gl.bind_buffer target bufid;
    Gl.buffer_data target byte_length (Some data) Gl.static_draw

  let load_mesh program json buffers mesh =
    let name = find mesh ["name"] in
    let primitives = find mesh ["primitives"] in
    let indices = get_string (find primitives ["indices"]) in
    let attributes = get_dict (find mesh ["attributes"]) in
    let vao = int_value (Gl.gen_vertex_arrays 1) in
    Gl.bind_vertex_array vao;
    bind_buffer ~target:Gl.element_array_buffer json buffers indices;
    List.iter (fun (attr_name, attr_value) ->
      let attr = find json ["accessors";attr_value] in
      let buffer_view = get_string (find attr ["bufferView"]) in
      let typ = get_int (find attr ["type"]) in
      bind_buffer json buffers buffer_view;
      let attrib = Gl.get_attrib_location program attr_name in
      if attrib = -1 then
        invalid_arg attr_name;
      (* TODO: vertex_attrib_ipointer *)
      let normalized =
        try get_bool (find attr ["normalized"]) with Not_found -> false in
      let stride = get_int (find attr ["byteStride"]) in
      let offset = get_int (find attr ["byteOffset"]) in
(*      let components, typ = parse_attr_type (find attr ["type"]) in
      Gl.vertex_attrib_pointer attrib components typ stride (`Offset offset)*)
failwith "TODO"
    ) attributes;
    Gl.bind_buffer Gl.array_buffer 0;
    Gl.bind_vertex_array 0;
    vao

    *)

  let print_string_opt out = function
    | None -> ()
    | Some s -> output_string out s

(*  let load_material material =
    Printf.eprintf "Loading material %a ...%!" print_string_opt material.name;
    match material.instance_technique with
    | None -> ()
    | Some t ->
        t.technique*)

  let with_uri parent uri f =
    M.with_uri_contents (M.uri_of_string ~parent uri) f

(*  let load_pass parent shaders pass =
    let open Pass in
    prerr_endline "program";
    let open Program in
    with_uri parent pass.program.vertex_shader (fun vstext ->
        with_uri parent pass.program.fragment_shader (fun fstext ->
            with_shader `GL_VERTEX_SHADER vstext (fun vs ->
                with_shader `GL_FRAGMENT_SHADER fstext (fun fs ->
                    let program = link_program [ fs; vs ] in
                    ()
                  )
              )
          )
      )*)

  let load_shader parent shaders key f =
    let path = (StringMap.find key shaders).Shader.path in
    Printf.eprintf "Shader '%s'...\n%!" path;
    with_uri parent path (fun text -> f ("#version 100\n" ^ text))

  let load_program parent shaders program =
    let open Program in
    load_shader parent shaders program.vertex_shader (fun vstext ->
        with_shader `GL_VERTEX_SHADER vstext (fun vs ->
            load_shader parent shaders program.fragment_shader (fun fstext ->
                with_shader `GL_FRAGMENT_SHADER fstext (fun fs ->
                    link_program [ fs; vs ]
                  )
              )
          )
      )

(*  let load_technique parent shaders name t =
    let open Technique in
    Printf.eprintf "Loading technique %s ... %!" name;
    let passes = StringMap.map (load_pass parent shaders) t.passes in
    Printf.eprintf "OK\n%!";
    ()*)

  let get_indices gltf key =
    GlTF.Indices.t_of_json (StringMap.find key gltf.accessors)

  let get_attribute gltf key =
    GlTF.MeshAttribute.t_of_json (StringMap.find key gltf.accessors)

  let bind_buffer gltf buffer_view byte_offset count bind_type =
   () 

  let load_primitive gltf primitive =
    let vao = gen_vertex_array () in
    with_bind_vao vao (fun () ->
      let indices = get_indices gltf primitive.indices in
      bind_buffer gltf indices.buffer_view indices.byte_offset indices.count indices.type_
    );
    vao

  let load_mesh gltf mesh =
    List.rev_map (load_primitive gltf) mesh.primitives


  let load path =
    Printf.eprintf "Loading %s ...%!" path;
    let parent = M.uri_of_string path in
    M.with_uri_contents parent (fun contents ->
      let json = from_string contents in
      try
        let gltf = GlTF.t_of_json json in
        Printf.eprintf "OK\n%!";
        let programs = StringMap.map (load_program parent gltf.shaders) gltf.programs in
        let meshes = StringMap.map (load_mesh gltf) gltf.meshes in
        ()
      with Ezjsonm.Parse_error (json, msg) ->
        if Printexc.backtrace_status () then
          Printexc.print_backtrace stderr;
        Printf.eprintf "%s in '%s'\n%!" msg
          (Ezjsonm.to_string (`A [json]))
    )
end
