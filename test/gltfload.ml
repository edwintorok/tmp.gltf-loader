open GltfLoader
open Tsdl
open Tgl3

module Loader = Make(struct
  type uri = string
  let uri_of_string ?parent str =
    match parent with
    | Some p ->
        Filename.concat (Filename.dirname p) str
    | None -> str

  let with_uri_contents uri f =
    (* TODO: with_resource *)
    let ch = open_in uri in
    let n = in_channel_length ch in
    let contents = String.make n ' ' in
    really_input ch contents 0 n;
    let r = f contents in
    close_in ch;
    r
end)

let check = function
  | `Ok r -> r
  | `Error msg -> failwith msg

let reshape ~w ~h =
  Gl.viewport 0 0 w h

let display () =
  ()

let rec loop win =
  let w, h = Sdl.get_window_size win in
  reshape ~w ~h;
  display ();
  if Gl.get_error () <> Gl.no_error then
    Sdl.log_warn Sdl.Log.category_application "OpenGL error";
  Sdl.gl_swap_window win

let () =
  check (Sdl.init Sdl.Init.video);
  check (Sdl.gl_set_attribute Sdl.Gl.context_major_version 3);
  check (Sdl.gl_set_attribute Sdl.Gl.context_minor_version 1);
  check (Sdl.gl_set_attribute Sdl.Gl.context_profile_mask Sdl.Gl.context_profile_core);
  let title = Sys.argv.(0) in
  let attrs = Sdl.Window.(opengl + resizable + shown) in
  let win = check (Sdl.create_window ~w:200 ~h:200 title attrs) in
  let ctx = check (Sdl.gl_create_context win) in
  check (Sdl.gl_make_current win ctx);

  let mesh = Loader.load "../glTF/model/duck/duck.json" in
  let mesh = Loader.load "../glTF/model/rambler/Rambler.json" in
  let mesh = Loader.load "../glTF/model/SuperMurdoch/SuperMurdoch.json" in
  let mesh = Loader.load "../glTF/model/wine/wine.json" in

  loop win;

  Sdl.gl_delete_context ctx;
  Sdl.destroy_window win;
  Sdl.quit ()
